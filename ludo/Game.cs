﻿using System;
using System.Collections.Generic;

namespace ludo
{
	internal class Game
	{
		private readonly List<Player> _players = new List<Player>();
		private readonly List<string> _playerNames = new List<string>();
		private int _playerNumber;
		
		public Game()
		{
			CreatePlayers();
			
			foreach (var playerName in this._playerNames)
			{
				this._players.Add(new Player(playerName));
			}

			for (var i = 0; i < _players.Count; i++)
			{
				switch (i)
				{
					case 0:
						this._players[0].Color = Color.Green;
						break;
					case 1:
						this._players[1].Color = Color.Yellow;
						break;
					case 2:
						this._players[2].Color = Color.Blue;
						break;
					case 3:
						this._players[3].Color = Color.Red;
						break;
				}
			}

			foreach (var player in _players)
			{
				Console.WriteLine($"#{player.ID} {player.Name} - {player.Color}");
			}
		}

		private void CreatePlayers()
		{
			Console.Write("Antal Spillere (2-4): ");

			while (this._playerNumber < 2 || this._playerNumber > 4)
			{
				if (!int.TryParse(Console.ReadLine(), out this._playerNumber) || this._playerNumber < 2 || this._playerNumber > 4)
				{
					Console.WriteLine("Du skal vælge et antal mellem 2 og 4");
				}
			}
			
			for (var i = 0; i < this._playerNumber; i++)
			{
				Console.Write($"Spiller {i+1} navn: ");
				this._playerNames.Add(Console.ReadLine());
			}
		}
	}
}