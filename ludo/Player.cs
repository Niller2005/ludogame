﻿using System;

namespace ludo
{
	public enum Color
	{
		Green,
		Yellow,
		Blue,
		Red
	}

	internal class Player
	{
		public string Name { get; }
		public Color Color { get; set; }
		public Guid ID { get; private set; }


		public Player(string name)
		{
			this.Name = name;
			this.ID = Guid.NewGuid();
		}
	}
}